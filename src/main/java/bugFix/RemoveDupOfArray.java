package bugFix;

import java.util.HashSet;
import java.util.Set;

public class RemoveDupOfArray {

	public static void main(String[] args) {
		
		Integer[]  num = {45, 45, 45, 67, 67,67, 78, 89, 23};
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < num.length; i++) {
			for (int j = i+1; j < num.length; j++) {
				set.add(num[j]);
			}
		}
		System.out.println(set);
	}
}

package bugFix;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Find {
	String sample = "<name>Belgian Waffles</name>\r\n" + 
			"    <price>$5.95</price>\r\n" + 
			"    <description>\r\n" + 
			"   Two of our famous Belgian Waffles with plenty of real maple syrup\r\n" + 
			"   </description>\r\n" + 
			"    <calories>650</calories>";

	@BeforeMethod
	public void before() {
		
	}
	@Test(dataProvider="fetchData")
	public void test(String tagName) {
		String[] split = sample.split("\\W");
		int length = split.length;
		for(int i=0;i<length;i++) {
			if(split[i].contains(tagName)) {
				for(int j=i+1;j<length;j++) {
					if(split[j].contains(tagName)) {
						break;
					}
					else
					{
						System.out.println(split[j]);
					}
				}break;
			}
		}
	}
	@DataProvider(name="fetchData")
	public Object[][] dp() throws IOException {
		return Data.data();
	}
}

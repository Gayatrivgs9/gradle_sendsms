package coding;

import java.util.Scanner;

public class UserInput {
	public static void main(String[] args) {
		//int salary[] = {23000, 45000, 67000, 89000};
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int size = sc.nextInt();
		int salaries[] = new int[size];
		System.out.println("Enter numbers");
		for(int i =0; i<salaries.length; i++) {
			salaries[i] = sc.nextInt();
		}
		/*for(int i =0; i<salary.length; i++) {
			System.out.println(salary[i]);
		}*/
		System.out.println("Array of numbers");
		for(int salary : salaries) {
			System.out.println(salary);
		}
		sc.close();
	}
}

